<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    @include('Partials.head')
</head>
<body>
<div class="container-fluid mb-5">
    <div class="row position-relative" style="background-color: #00b2f3">
        {{--<div class="p-2 position-absolute abs-custom">
            <a href="{{ url('/') }}" class="float-left pl-3 pr-3"><h4><i class="fa fa-arrow-left text-white"></i></h4>
            </a>
        </div>--}}
        <div class="col-12 pt-2 pb-2">
            <h2 class="text-center text-white mb-0">Silahkan Pilih Merk Kendaraan Anda</h2>
        </div>
    </div>
    {{--<div class="row bg-black-trans pt-1 pb-1">
        <div class="col-md-12 position-relative">
            <div class="runningtext full">
                <h5>Promosikan usaha/produk anda di sini dengan menggunakan iklan running text. Promosikan usaha/produk
                    anda di sini dengan menggunakan iklan running text. </h5>
            </div>
        </div>
    </div>--}}
</div>
<div class="container" style="margin: 0px 40px 40px 40px">
    <div class="row">
        <div class="col-md-8">
            <div class="row" style="height:100%;">
            {{--@foreach ($merks[0] as $m)--}}
            {{--<div class="col-md-3 slow fadeIn animated" style="margin: 0px; height: 160px">--}}
            {{--<a href="{{ url('type').'/'.$m->id}}" class=" position-relative">--}}
            {{--<div class="card custom mb-2 text-center mb-5" style="height: 140px">--}}
            {{--<img class="img-merk" src="{{$m->gambar}}">--}}
            {{--<div class="card-body">--}}
            {{--<h5 class="card-title">{{$m->keterangan}}</h5>--}}
            {{--</div>--}}
            {{--</div>--}}
            {{--</a>--}}
            {{--</div>--}}
            {{--@endforeach--}}

            <!--Carousel Wrapper-->
                <div id="multi-item" class="carousel slide carousel-multi-item animated slow fadeIn mb-0"
                     data-ride="carousel">

                    <!--Controls-->
                    <div class="controls-top custom">
                        <a class="btn-floating" href="#multi-item" data-slide="prev"><i class="fas fa-chevron-left"></i></a>
                        <a class="btn-floating" href="#multi-item" data-slide="next"><i
                                    class="fas fa-chevron-right"></i></a>
                    </div>
                    <!--/.Controls-->

                    <!--Slides-->
                    <div class="carousel-inner" role="listbox">
                    <?php $no = 1;?>
                    @foreach ($merks as $cc)
                        <!--slide-->
                            <?php if ($no == 1) {
                                $class = 'active';
                            } else {
                                $class = '';
                            }?>
                            <div class="carousel-item pl-4 pr-4 <?= $class?>">
                                <!--content-->
                                @foreach ($cc as $v)
                                    <div class="col-md-3 slow fadeIn animated" style="margin: 0px; height: 160px">
                                        <a href="{{ url('type').'/'.$v->id}}" class=" position-relative">
                                            <div class="card custom mb-2 text-center mb-5 d-table-cell align-middle"
                                                 style="width:150px; height: 140px">
                                                <img class="img-merk" src="{{$v->gambar}}">
                                                <div class="card-body">
                                                    {{--<h5 class="card-title">{{$m->keterangan}}</h5>--}}
                                                </div>
                                            </div>
                                        </a>
                                    </div>
                                    <!--/.content-->
                                @endforeach

                            </div>
                            <!--/.slide-->
                            <?php $no++;?>
                        @endforeach
                    </div>
                    <!--/.Slides-->

                </div>
                <!--/.Carousel Wrapper-->
            </div>
        </div>
        <div class="col-md-4" style="">
            <div class="code" style="display:flex; align-items:center;">
                <div class="row" style="margin:0px !important;">
                    <h4 class="col-sm-11 col-centered input-group font-weight-bolder" style="margin-bottom:6px;">Scan
                        Kode Booking : </h4>

                        <form method="post" name="cetak-boking" class="col-sm-11 col-centered input-group ">
                            <input type="text" id="kodebooking" class="form-control align-middle"
                                   placeholder="Kode Booking......">
                            <span class="input-group-btn">
                            <button type="submit" id="cetak" class="btn btn-info btn-flat"
                                    style="margin:0px !important;"><i class="fa fa-print"></i></button>
                        </span>
                        </form>
                </div>
            </div>
            <div style="margin-left: 50px; margin-bottom: 15px;">
                <img src="https://via.placeholder.com/400x330.png?text=Contoh+Iklan" class="center"
                     style="border-radius: 10%;">
            </div>
        </div>
    </div>
</div>

<div id="domMessage" style="display:none;">
    <h5 style="float: none; margin: 0 auto;"><img src="{{asset('assets/images/loading.gif')}}"/> Transaksi sedang
        diproses ...</h5>
</div>
@include('Partials.footer')
<
<script>
    $(function () {
        $('#kodebooking').focus();
        $('#multi-item').carousel({
            interval: false
        });
    });

    function showAlert(message) {
        $.confirm({
            title: 'Perhatian',
            content: message,
            buttons: {
                confirm: {
                    text: 'Ok',
                    btnClass: 'col-md-12 btn blue-gradient',
                    action: function () {
                        $('#kodebooking').val('');
                        $('#kodebooking').focus();
                    }
                }
            }
        });
    }

    $("#cetak").on("click", function (e) {
        e.preventDefault();
        if ($('#kodebooking').val() == null || $('#kodebooking').val() == '') {
            showAlert('Masukan kode booking terlebih dahulu');
        } else {
            $.blockUI({message: $('#domMessage')});
            $.ajax({
                method: "POST",
                url: "<?= url('/print'); ?>",
                data: {
                    code: $('#kodebooking').val()
                },
                success: function (resp) {
                    // console.log(resp);

                    $.unblockUI();
                    if (resp.error) {
                        showAlert(resp.msg);
                    }
                    $('#kodebooking').val('');
                    $('#kodebooking').focus();
                },
                error: function (data) {
                    $.unblockUI();
                    alert("Something Gone Wrong");
                }
            });
        }
    });
</script>
</body>
</html>
