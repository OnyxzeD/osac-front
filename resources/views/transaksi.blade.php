<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    @include('Partials.head')
</head>
<body class="transaksi">

<div class="container-fluid">
    <div class="row position-relative mb-0" style="background-color: #00b2f3">
        <div class="p-2 position-absolute abs-custom">
            <a href="{{ url('type').'/'.$merk }}" class="float-left pl-3 pr-3"><h4><i
                            class="fa fa-arrow-left text-white"></i>
                </h4></a>
            <input type="hidden" value="{{$kendaraan}}" id="kid">
            <input type="hidden" value="{{$jamTutup}}" id="hours">
        </div>
        <div class="col-12 pt-2 pb-2">
            <h1 class="text-center text-white mb-0">Silahkan Pilih Layanan yang Anda Inginkan</h1>
        </div>
    </div>
    <div class="row" style="height:100%;">
        <div class="col-9 pt-0 pl-3 pr-3 pb-3 border border-grey col-custom-1 position-relative">
        {{--<div class="runningtext custom bg-black-trans mb-0"><h5>Running Text Iklan</h5></div>--}}
        <!--Carousel Wrapper-->
            <div id="multi-item-example" class="carousel slide carousel-multi-item mt-3 animated slow fadeIn"
                 data-ride="carousel" style="margin-bottom: 5px;">

                <!--Controls-->
                <div class="controls-top custom">
                    <a class="btn-floating" href="#multi-item-example" data-slide="prev"><i
                                class="fas fa-chevron-left"></i></a>
                    <a class="btn-floating" href="#multi-item-example" data-slide="next"><i
                                class="fas fa-chevron-right"></i></a>
                </div>
                <!--/.Controls-->

                <!--Slides-->
                <div class="carousel-inner" role="listbox" style="margin-top: 0px">
                    <p class="card-text service" style="font-weight: bold"> Carwash </p>
                <?php $no = 1;?>
                @foreach ($carwash as $cw)
                    <!--slide-->
                        <?php if ($no == 1) {
                            $class = 'active';
                        } else {
                            $class = '';
                        }?>
                        <div class="carousel-item pl-4 pr-4 <?= $class?>">
                            <!--content-->
                            @foreach ($cw as $v)
                                <div class="col-item col-md-4 carwash" id="carwash-{{$v->id}}"
                                     onclick="buy(this, '{{$v->id}}', '{{$v->nama}}','{{$v->harga}}')" style="">
                                    <a href="#" class="remove-mini"></a>
                                    <a href="#" class=" position-relative">
                                        <div class="card" style="height: 100px;">
                                            <div class="card-body col-centered"
                                                 style="display:flex; align-items:center;">
                                                {{--<img src="{{$v->gambar}}" style="height: 80px; width: auto">--}}
                                                <h4 class="text-center font-weight-bolder">{{ucwords(strtolower($v->nama))}}
                                                    <br>
                                                    {{"Rp " . number_format($v->harga, 0, ',', '.')}}</h4>
                                            </div>
                                        </div>
                                    </a>
                                </div>
                                <!--/.content-->
                            @endforeach

                        </div>
                        <!--/.slide-->
                        <?php $no++;?>
                    @endforeach
                </div>
                <!--/.Slides-->

            </div>
            <!--/.Carousel Wrapper-->

            <!--Carousel Wrapper-->
            <div id="multi-item" class="carousel slide carousel-multi-item animated slow fadeIn mb-0"
                 data-ride="carousel">

                <!--Controls-->
                <div class="controls-top custom">
                    <a class="btn-floating" href="#multi-item" data-slide="prev"><i class="fas fa-chevron-left"></i></a>
                    <a class="btn-floating" href="#multi-item" data-slide="next"><i
                                class="fas fa-chevron-right"></i></a>
                </div>
                <!--/.Controls-->

                <!--Slides-->
                <div class="carousel-inner" role="listbox">
                    <p class="card-text service" style="font-weight: bold"> Carcare </p>
                <?php $no = 1;?>
                @foreach ($carcare as $cc)
                    <!--slide-->
                        <?php if ($no == 1) {
                            $class = 'active';
                        } else {
                            $class = '';
                        }?>
                        <div class="carousel-item pl-4 pr-4 <?= $class?>" style="min-height:450px !important;">
                            <!--content-->
                            @foreach ($cc as $v)
                                <div class="col-item col-md-3 carcare"
                                     onclick="buy(this, '{{$v->id}}', '{{$v->nama}}','{{$v->harga}}')">
                                    {{--<a href="#" class="remove"><i class="fas fa-times"></i></a>--}}
                                    <?php
                                    date_default_timezone_set('Asia/Jakarta');
                                    $end_time = strtotime($jamTutup);
                                    $cur_time = strtotime(date("H:i"));

                                    if ($cur_time > $end_time) {
                                    ?>
                                    <a href="#" class="closed">
                                        <b class="col-centered">TUTUP</b>
                                    </a>
                                    <?php } else {?>
                                    <a href="#" class="remove"></a>
                                    <?php }?>
                                    <a href="#" class=" position-relative">
                                        <div class="card" style="height: 200px;">
                                            <div class="card-body col-centered">
                                                <img src="{{$v->gambar}}" style="height: 100px; width: auto">
                                            </div>
                                            <div class="card-footer" style="height: 100px;">
                                                <p class="card-text price"
                                                   style="font-weight: bolder">{{ucwords(strtolower($v->nama))}} <br>
                                                    {{"Rp " . number_format($v->harga, 0, ',', '.')}}</p>
                                            </div>
                                        </div>
                                    </a>
                                </div>
                                <!--/.content-->
                            @endforeach

                        </div>
                        <!--/.slide-->
                        <?php $no++;?>
                    @endforeach
                </div>
                <!--/.Slides-->

            </div>
            <!--/.Carousel Wrapper-->

        </div>

        <div class="col-3 p-0 border border-grey position-relative">
            <div class="row m-0  col-custom-2">
                <div class="col-12 p-3">
                    <div class="form-group text-center">
                        <input type="hidden" class="form-control form-custom-1 text-center" id="platnomor"
                               placeholder="MASUKKAN PLAT NOMOR">
                    </div>
                    <table class="table table-custom-1 list-item-jasa" id="cart">

                    </table>
                </div>
            </div>
            <div class="row m-0 position-absolute row-custom-1">
                <div class="col-12 pl-0 pr-0">
                    <table class="table table-custom-1">
                        <tr>
                            <td><h4 class="mb-0">Total</h4></td>
                            <td><h4 class="mb-0" id="total"><b>Rp 0</b></h4></td>
                        </tr>
                    </table>
                </div>
                <div class="col-12 pl-0 pr-0 pb-2 pt-3 text-center">
                    <button class="btn btn-primary btn-custom-2" style="background-color: #00b2f3" id="confirm">PESAN
                    </button>
                </div>
            </div>
        </div>
    </div>
</div>

<div id="domMessage" style="display:none;">
    <h5 style="float: none; margin: 0 auto;"><img src="{{asset('assets/images/loading.gif')}}"/> Transaksi sedang
        diproses ...</h5>
</div>

@include('Partials.footer')
<script type="text/javascript">
    var url_confirm = "<?= url('/save'); ?>";
    var url_done = "<?= url('/'); ?>";
    var services = [];
    var total = 0;
    var carwash = null;

    $(function () {
        $('.carousel').carousel({
            interval: false
        });
    });

    $(".col-item").on("click", function (e) {
        e.preventDefault();
    });

    function rupiah(value) {
        var sisa = value.toString().length % 3,
            rupiah = value.toString().substr(0, sisa),
            ribuan = value.toString().substr(sisa).match(/\d{3}/g);

        if (ribuan) {
            separator = sisa ? '.' : '';
            rupiah += separator + ribuan.join('.');
        }

        return rupiah;
    }

    function timeCheck() {
        var d = new Date();
        var h = ((d.getHours() < 10) ? '0' + d.getHours() : d.getHours());
        var m = ((d.getMinutes() < 10) ? '0' + d.getMinutes() : d.getMinutes());
        var now = h + ":" + m + ":00";

        console.log(Date.parse('01/01/2019 ' + now) < Date.parse('01/01/2019 ' + $('#hours').val() + ":00"));
        return Date.parse('01/01/2019 ' + now) < Date.parse('01/01/2019 ' + $('#hours').val() + ":00");
    }

    function buy(element, id, service, price) {

        if ($(element).hasClass("selected")) {
            if ($(element).hasClass("carwash") && carwash != null) {
                carwash = null;
            }
            $(element).removeClass('selected');
            $("#" + id).remove();
            total -= parseInt(price);
            $("#total").text("Rp " + rupiah(total));
            services.splice($.inArray(parseInt(id), services), 1);
            // console.log(services)
        } else {
            if ($(element).hasClass("carwash")) {
                if (carwash != null) {
                    $("#carwash-" + carwash).click();
                }

                carwash = id;
                $(element).addClass('selected');
                $('#cart').append(
                    "<tr id='" + id + "'>" +
                    "   <td> <span class=\"quantity\">1</span> </td>" +
                    "   <td>" + service + "</td>" +
                    "   <td><span class=\"price\">Rp " + rupiah(price) + "</span></td>" +
                    "</tr>");

                total += parseInt(price);
                $("#total").text("Rp " + rupiah(total));
                services.push(parseInt(id));
                // console.log(services);
            } else if ($(element).hasClass("carcare")) {
                if (timeCheck()) {
                    carwash = 'selected';
                    $(element).addClass('selected');
                    $('#cart').append(
                        "<tr id='" + id + "'>" +
                        "   <td> <span class=\"quantity\">1</span> </td>" +
                        "   <td>" + service + "</td>" +
                        "   <td><span class=\"price\">Rp " + rupiah(price) + "</span></td>" +
                        "</tr>");

                    total += parseInt(price);
                    $("#total").text("Rp " + rupiah(total));
                    services.push(parseInt(id));
                }
            }
        }
    }

    $('#confirm').on('click', function () {

        if (services.length > 0) {
            $.blockUI({message: $('#domMessage')});
            $.ajax({
                method: "POST",
                url: url_confirm,
                data: {
                    id_kendaraan: parseInt($('#kid').val()),
                    services: JSON.stringify(services)
                },
                cache: false,
                success: function (resp) {
                    // alert(resp.msg);
                    $.unblockUI();
                    window.location = url_done;
                },
                error: function (data) {
                    console.log(data);
                    $.unblockUI();
                    // alert(data.msg);
                }
            });
        } else {
            $.confirm({
                title: 'Perhatian',
                content: 'Mohon pilih jasa terlebih dahulu',
                buttons: {
                    confirm: {
                        text: 'Ok',
                        btnClass: 'col-md-12 btn blue-gradient',
                        action: function () {

                        }
                    }
                }
            });
        }
    });

    $('.carousel').carousel({
        interval: false
    })
</script>
</body>
</html>
